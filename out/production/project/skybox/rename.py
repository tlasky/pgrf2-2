import os


for file in os.listdir():
	if not os.path.isfile(file) or  file.endswith('.py') or '_' not in file:
		continue
	new_file = file.split('_')[1].lower()
	print(file, '=>', new_file)
	os.rename(file, new_file)


for file in os.listdir():
	print('"/skybox/{}",'.format(file))
