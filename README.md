# PGRF2-2

Druhá úloha PGRF2

8. týden test (příští týden)
11. týden test
9-10 obhajoby projektu (prezentace - 5 sl)

https://gitlab.com/honza.vanek/transforms.git
https://gitlab.com/honza.vanek/oglutils.git
http://oliva.uhk.cz/bbcswebdav/pid-101442-dt-content-rid-184538_1/courses/KIKM-PGRF2/jogl/JakNaGitJOGLaJAR.pdf
http://oliva.uhk.cz/webapps/blackboard/content/listContent.jsp?course_id=_769_1&content_id=_101008_1&mode=reset


    Skybox - OK
        - Přechod ze sférického na klasický skybox.
    Kolize - OK
        - (Odstranění problému s pozicí kamery.)
    Optimalizace - OK
        - gl.glDepthFunc(GL2.GL_LESS);
        - gl.glEnable(GL2.GL_CULL_FACE);
        - Vykreslování budov v rozsahu pomocí Utils.inCircle(...);
        - Celkově mnohem více FPS...
    Kamera - ~
        - O něco lepší řešení....
    