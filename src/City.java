import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.gl2.GLUT;
import transforms.Point3D;

import java.util.ArrayList;
import java.util.List;

public class City implements Drawable {
    private int size = 1000;

    private int chunkCount = size / 25;
    private float chunkSize = size / chunkCount;
    private List<Chunk> chunks = new ArrayList<>();

    City() {
        for (int i = 1; i < chunkCount; i++) {
            for (int j = 1; j < chunkCount; j++) {
                chunks.add(new Chunk(
                        chunkSize,
                        (-size / 2f) + (i * chunkSize),
                        (-size / 2f) + (j * chunkSize)
                ));
            }
        }
    }


    private void drawGround(GL2 gl, Textures textures) {
        int groundSize = size + 300;

        textures.set("grass.png");
        gl.glPushMatrix();
        gl.glTranslatef(-groundSize / 2f, -groundSize / 2f, 0);
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(0f, 0f);
        gl.glVertex3f(0f, groundSize, 0f);
        gl.glTexCoord2f(0f, 1f);
        gl.glVertex3f(0f, 0f, 0f);
        gl.glTexCoord2f(1f, 1f);
        gl.glVertex3f(groundSize, 0f, 0f);
        gl.glTexCoord2f(1f, 0f);
        gl.glVertex3f(groundSize, groundSize, 0f);
        gl.glEnd();
        gl.glPopMatrix();
        textures.disable();
    }

    public void drawSkybox(GL2 gl, Textures textures) {
        gl.glEnable(GL2.GL_TEXTURE_2D);
        gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_REPLACE);

        float boxSize = size + 300;

        textures.set("skybox_bk.jpg");
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3d(-boxSize, -boxSize, -boxSize);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3d(-boxSize, boxSize, -boxSize);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3d(-boxSize, boxSize, boxSize);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3d(-boxSize, -boxSize, boxSize);
        gl.glEnd();

        textures.set("skybox_ft.jpg");
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3d(boxSize, boxSize, -boxSize);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3d(boxSize, -boxSize, -boxSize);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3d(boxSize, -boxSize, boxSize);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3d(boxSize, boxSize, boxSize);
        gl.glEnd();

        textures.set("skybox_lf.jpg");
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3d(boxSize, -boxSize, -boxSize);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3d(-boxSize, -boxSize, -boxSize);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3d(-boxSize, -boxSize, boxSize);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3d(boxSize, -boxSize, boxSize);
        gl.glEnd();

        textures.set("skybox_rt.jpg");
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3d(-boxSize, boxSize, -boxSize);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3d(boxSize, boxSize, -boxSize);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3d(boxSize, boxSize, boxSize);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3d(-boxSize, boxSize, boxSize);
        gl.glEnd();

        /* Není potřeba
        textures.set("skybox_dn.jpg");
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3d(-boxSize, boxSize, -boxSize);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3d(-boxSize, -boxSize, -boxSize);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3d(boxSize, -boxSize, -boxSize);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3d(boxSize, boxSize, -boxSize);
        gl.glEnd();*/

        textures.set("skybox_up.jpg");
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex3d(-boxSize, -boxSize, boxSize);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex3d(-boxSize, boxSize, boxSize);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex3d(boxSize, boxSize, boxSize);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex3d(boxSize, -boxSize, boxSize);
        gl.glEnd();

        textures.disable();
    }

    private void drawStreets(GL2 gl, Textures textures, boolean vertical) {
        float width = 3;
        float length = size;
        float count = chunkCount;

        float x = (size / 2f) - (width / 2);
        float y = -size / 2f;
        float totalWidth = width * count;
        float totalOffset = size - totalWidth;
        float offset = totalOffset / count;

        for (int i = 0; i <= count; i++) {
            textures.set("street.jpg");
            gl.glPushMatrix();
            if (vertical) {
                gl.glRotatef(90, 0, 0, 1);
            }
            gl.glTranslatef(x, y, 1);
            gl.glRectf(0, 0, width, length);
            gl.glPopMatrix();
            textures.disable();

            x -= offset + width;
        }
    }

    boolean hits(Point3D cam) {
        if (cam.getX() < -size / 2 || cam.getX() > size / 2) {
            return true;
        }
        if (cam.getY() < -size / 2 || cam.getY() > size / 2) {
            return true;
        }
        if (cam.getZ() > size / 2) {
            return true;
        }
        for (Chunk chunk : chunks) {
            if (chunk.isIn(cam) && chunk.hits(cam)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void draw(GL2 gl, GLU glu, GLUT glut, Textures textures, Point3D cam) {
        drawGround(gl, textures);
        drawStreets(gl, textures, false);
        drawStreets(gl, textures, true);

        for (Chunk chunk : chunks) {
            if (Utils.inCircle(
                    (float)cam.getX(),
                    (float)cam.getY(),
                    400,
                    chunk.getX(),
                    chunk.getY())
            ) {
                chunk.draw(gl, glu, glut, textures, cam);
            }
        }
    }

    public int getSize() {
        return size;
    }
}
