import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;

class Textures {
    /*
     *  Pro lepší práci s texturama...
     * */
    private GL2 gl;
    private HashMap<String, Texture> map = new HashMap<>();
    private String missingTexture = "/missing.png";

    Textures(GL2 gl, String[] filenames) {
        this.gl = gl;
        load(filenames);
    }

    private String textureNameFromFileName(String filename) {
        // Generování jmen pro textury...
        if (filename.startsWith("/")) {
            filename = filename.substring(1);
        }
        return filename.replaceAll("/", "_");
    }

    private void loadTexture(String filename) {
        String texturename = textureNameFromFileName(filename);
        System.out.printf("Loading \"%s\" as \"%s\"... ", filename, texturename);
        InputStream is = getClass().getResourceAsStream(filename);
        if (is == null) {
            System.out.println("File not found");
        } else {
            try {
                map.put(texturename, TextureIO.newTexture(is, true, Utils.splitExtension(filename)));
            } catch (GLException | IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("OK");
    }

    private void load(String[] filenames) {
        if (!Arrays.asList(filenames).contains(missingTexture)) {
            loadTexture(missingTexture);
        }
        for (String filename : filenames) {
            loadTexture(filename);
        }
    }

    private Texture get(String texturename) {
        if (map.containsKey(texturename)) {
            return map.get(texturename);
        } else {
            /* Debug... */
            System.out.printf("Missing texture \"%s\"!\n", texturename);
            return map.get(textureNameFromFileName(missingTexture));
        }
    }

    void set(String texturename) {
        Texture t = get(texturename);
        t.enable(gl);
        t.bind(gl);
    }

    void disable() {
        gl.glDisable(GL2.GL_TEXTURE_2D);
    }
}
