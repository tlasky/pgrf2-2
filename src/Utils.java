class Utils {
    public Utils(){}

    static String splitExtension(String path) {
        String[] splits = path.split("\\.");
        String extension = "";
        if(splits.length >= 2)
        {
            extension = splits[splits.length-1];
        }
        return extension;
    }

    static float randomRange(float min, float max) {
        return (float) (min + Math.random() * (max - min));
    }

    static int randomRange(int min, int max) {
        return (int) (min + Math.random() * (max - min));
    }

    static boolean inCircle(float center_x, float center_y, float radius, float x, float y) {
        double square_dist = Math.pow(center_x - x, 2) + Math.pow(center_y - y, 2);
        return square_dist <= Math.pow(radius, 2);
    }

    static boolean chance(float percentage) {
        if (Math.random() * 100 < percentage) {
            return true;
        } else {
            return false;
        }
    }
}
