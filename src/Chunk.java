import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.gl2.GLUT;
import transforms.Point3D;

import java.util.ArrayList;
import java.util.List;

public class Chunk implements Drawable {
    private float size;
    private float x, y, z;

    private List<Building> buildings = new ArrayList<>();

    Chunk(float size, float x, float y) {
        this.size = size;
        this.x = x;
        this.y = y;
        this.z = 0;
        init();
    }

    private void init() {
        float percentage = 30;

        if (Utils.chance(percentage)) {
            buildings.add(new Building(
                    x + Utils.randomRange(5, 8),
                    y + Utils.randomRange(5, 8),
                    z
            ));
        }

        if (Utils.chance(percentage)) {
            buildings.add(new Building(
                    x + size - Utils.randomRange(5, 8),
                    y + Utils.randomRange(5, 8),
                    z
            ));
        }

        if (Utils.chance(percentage)) {
            buildings.add(new Building(
                    x + Utils.randomRange(5, 8),
                    y + size - Utils.randomRange(5, 8),
                    z
            ));
        }

        if (Utils.chance(percentage)) {
            buildings.add(new Building(
                    x + size - Utils.randomRange(5, 8),
                    y + size - Utils.randomRange(5, 8),
                    z
            ));
        }
    }

    boolean isIn(Point3D cam) {
        return cam.getX() > x && cam.getX() < x + size && cam.getY() > y && cam.getY() < y + size;
    }

    boolean hits(Point3D cam) {
        for (Building building : buildings) {
            if (building.hits(cam)) {
                return true;
            }
        }
        return cam.getZ() <= z;
    }

    @Override
    public void draw(GL2 gl, GLU glu, GLUT glut, Textures textures, Point3D cam) {
        for (Building building : buildings) {
            building.draw(gl, glu, glut, textures, cam);
        }
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }
}
