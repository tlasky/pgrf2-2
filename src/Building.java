import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.gl2.GLUT;
import transforms.Point3D;

import java.util.ArrayList;
import java.util.List;

public class Building implements Drawable {
    private float x, y, z;
    private float height = Utils.randomRange(2, 8);

    private float floorHeight = 5;
    private float floorSize = 7;
    private List<BuildingFloor> floors = new ArrayList<>();

    private String[] textures = new String[]{
            "building_level.jpg",
            "building_level_normal.jpg",
            "building_level_industrial.jpg"
    };
    private String texture = textures[Utils.randomRange(0, textures.length)];


    Building(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;

        for (float i = z; i < height; i++) {
            floors.add(new BuildingFloor(
                    x, y,
                    floorHeight * i
            ));
        }
    }

    private void drawFlatRoof(GL2 gl, Textures textures) {
        textures.set("building_roof.jpg");
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        gl.glBegin(gl.GL_QUADS);
        gl.glTexCoord2f(0, 0);
        gl.glVertex3f(floorSize / 2, floorSize / 2, floorHeight * height);
        gl.glTexCoord2f(0, 1);
        gl.glVertex3f(-floorSize / 2, floorSize / 2, floorHeight * height);
        gl.glTexCoord2f(1, 1);
        gl.glVertex3f(-floorSize / 2, -floorSize / 2, floorHeight * height);
        gl.glTexCoord2f(1, 0);
        gl.glVertex3f(floorSize / 2, -floorSize / 2, floorHeight * height);
        gl.glEnd();
        gl.glPopMatrix();
        textures.disable();
    }

    @Override
    public void draw(GL2 gl, GLU glu, GLUT glut, Textures textures, Point3D cam) {
        for (BuildingFloor floor : floors) {
            floor.draw(gl, glu, glut, textures, cam);
        }
        drawFlatRoof(gl, textures);
    }

    boolean hits(Point3D cam) {
        return cam.getZ() <= floorHeight * height;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    // Jednotlivá patra
    private class BuildingFloor implements Drawable {
        private float x, y, z;

        BuildingFloor(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        @Override
        public void draw(GL2 gl, GLU glu, GLUT glut, Textures textures, Point3D cam) {
            textures.set(texture);
            gl.glPushMatrix();
            gl.glTranslatef(x, y, z);

            for (int i = 0; i < 4; i++) {
                gl.glRotatef(90 * i, 0, 0, 1);

                gl.glBegin(gl.GL_QUADS);
                gl.glTexCoord2f(0, 0);
                gl.glVertex3f(-floorSize / 2, floorSize / 2, floorHeight);
                gl.glTexCoord2f(0f, 1f);
                gl.glVertex3f(floorSize / 2, floorSize / 2, floorHeight);
                gl.glTexCoord2f(1f, 1f);
                gl.glVertex3f(floorSize / 2, floorSize / 2, 0);
                gl.glTexCoord2f(1f, 0f);
                gl.glVertex3f(-floorSize / 2, floorSize / 2, 0);
                gl.glEnd();
            }

            gl.glPopMatrix();
            textures.disable();
        }
    }
}
