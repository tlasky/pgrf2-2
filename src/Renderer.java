import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.gl2.GLUT;
import oglutils.OGLUtils;
import transforms.Point3D;

import java.awt.event.*;


class Renderer implements GLEventListener, MouseListener, MouseMotionListener, KeyListener {
    private GL2 gl;
    private GLU glu;
    private GLUT glut;

    private Textures textures;

    private int width, height;
    private int ox, oy;

    private float zenith;
    private float azimuth;
    private double px, py, pz;
    private float trans = 0;
    private long oldMils = System.currentTimeMillis();
    private float m[] = new float[16];
    private float m1[] = new float[16];

    private City city = new City();
    private boolean free = false;
    private boolean hits;
    private int fails = 0;


    @Override
    public void init(GLAutoDrawable glDrawable) {
        gl = glDrawable.getGL().getGL2();
        glu = new GLU();
        glut = new GLUT();

        OGLUtils.printOGLparameters(gl);

        gl.glEnable(GL2.GL_DEPTH_TEST);
        gl.glDisable(GL2.GL_LIGHTING);

        /* Optimalizace. */
        gl.glDepthFunc(GL2.GL_LESS);
        gl.glEnable(GL2.GL_CULL_FACE);

        gl.glFrontFace(GL2.GL_CCW);
        gl.glPolygonMode(GL2.GL_FRONT, GL2.GL_FILL);
        gl.glPolygonMode(GL2.GL_BACK, GL2.GL_FILL);

        textures = new Textures(gl, new String[]{
                "/skybox/bk.jpg",
                "/skybox/dn.jpg",
                "/skybox/ft.jpg",
                "/skybox/lf.jpg",
                "/skybox/rt.jpg",
                "/skybox/up.jpg",

                "/grass.png",
                "/street.jpg",
                "/building_level.jpg",
                "/building_level_industrial.jpg",
                "/building_level_normal.jpg",
                "/building_roof.jpg"
        });

        restart(gl);
    }

    private void restart(GL2 gl) {
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
        gl.glRotatef(-90, 1, 0, 0);

        /* Začáteční pozice. */
        gl.glRotatef(-90, 0, 0, 1);
        gl.glTranslatef(
                -city.getSize() / 4,
                -city.getSize() / 4,
                -100
        );
        hits = false;
    }

    @Override
    public void display(GLAutoDrawable glDrawable) {
        long mils = System.currentTimeMillis();
        float step = (mils - oldMils) / 1000.0f;
        oldMils = mils;
        trans = 20 * step;

        gl.glClearColor(0f, 0f, 0f, 1f);
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();

        glu.gluPerspective(45, width / (float) height, 0.1f, 3000.0f);

        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_REPEAT);
        gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_REPEAT);
        gl.glTexEnvi(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_REPLACE);


        gl.glMatrixMode(GL2.GL_MODELVIEW);

        gl.glPushMatrix();
        gl.glGetFloatv(GL2.GL_MODELVIEW_MATRIX, m1, 0);
        gl.glLoadIdentity();
        gl.glRotatef(-zenith, 1.0f, 0.0f, 0.0f);
        gl.glRotatef(azimuth, 0.0f, 1.0f, 0.0f);
        m1[12] = 0;
        m1[13] = 0;
        m1[14] = 0;
        gl.glMultMatrixf(m1, 0);
        city.drawSkybox(gl, textures);
        gl.glPopMatrix();

        gl.glGetFloatv(GL2.GL_MODELVIEW_MATRIX, m, 0);
        gl.glLoadIdentity();

        gl.glRotatef(-zenith, 1.0f, 0.0f, 0.0f);
        gl.glRotatef(azimuth, 0.0f, 1.0f, 0.0f);
        gl.glTranslated(-px, -py, -pz);
        gl.glMultMatrixf(m, 0);

        zenith = 0;
        azimuth = 0;
        px = 0;
        py = 0;
        if (!free && !hits) {
            pz = -trans;
        } else {
            pz = 0;
        }

        gl.glMatrixMode(GL2.GL_MODELVIEW);
        city.draw(gl, glu, glut, textures, getCameraPos());

        hits = city.hits(getCameraPos());
        if (hits) {
            restart(gl);
            fails++;
        }

        System.out.printf(
                "Collision: %b\tFails: %d\tPosition: %s\n",
                city.hits(getCameraPos()),
                fails,
                getCameraPos().toString()
        );
    }

    private Point3D getCameraPos() {
        return new Point3D(
                -(m[0] * m[12] + m[1] * m[13] + m[2] * m[14]),
                -(m[4] * m[12] + m[5] * m[13] + m[6] * m[14]),
                -(m[8] * m[12] + m[9] * m[13] + m[10] * m[14])
        );
    }

    private void withZenith(float value) {
        zenith += value;
        if (zenith > 90) {
            zenith = 90;
        }
        if (zenith <= -90) {
            zenith = -90;
        }
    }

    private void withAzimuth(float value) {
        azimuth += value;
        azimuth = azimuth % 360;
    }

    @Override
    public void reshape(GLAutoDrawable glDrawable, int x, int y, int width, int height) {
        this.width = width;
        this.height = height;
        glDrawable.getGL().getGL2().glViewport(0, 0, width, height);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        ox = e.getX();
        oy = e.getY();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (!hits && free) {
            int dx, dy;
            dx = e.getX() - ox;
            dy = e.getY() - oy;
            ox = e.getX();
            oy = e.getY();

            withZenith(dy);
            withAzimuth(dx);
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_C) {
            free = !free;
        }

        if (!hits && free) {
            if (e.getKeyCode() == KeyEvent.VK_CONTROL) {
                py -= trans;
            }
            if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                py += trans;
            }
            if (e.getKeyCode() == KeyEvent.VK_W) {
                pz -= trans;
            }
            if (e.getKeyCode() == KeyEvent.VK_S) {
                pz += trans;
            }
            if (e.getKeyCode() == KeyEvent.VK_A) {
                px -= trans;
            }
            if (e.getKeyCode() == KeyEvent.VK_D) {
                px += trans;
            }
        } else if (!hits && !free) {
            float step = 1.5f;
            if (e.getKeyCode() == KeyEvent.VK_A) {
                withAzimuth(-step);
            }
            if (e.getKeyCode() == KeyEvent.VK_D) {
                withAzimuth(step);
            }
            if (e.getKeyCode() == KeyEvent.VK_W) {
                withZenith(-step);
            }
            if (e.getKeyCode() == KeyEvent.VK_S) {
                withZenith(step);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
    }
}